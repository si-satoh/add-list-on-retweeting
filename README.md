# 目的
Twitter運用のちょっとした作業を簡略化するためのスクリプトです。  
cronに設定するほどではないような、短期間で実施されるイベントで、イベント参加者を非公開リストにまとめて管理するために作りました。

まずは  
*特定のツイートをリツイートしたユーザーをリストに入れる*  
項目を実装します。

ゆくゆくは  
*任意の条件を満たすユーザーをリストに入れる*  
ように実装します。

**注意事項**  

*  Composerを利用します  
*  OAuthの設定を済ませていることを前提とします  
*  TwitterのAPI制限のため、短期間に実行するとすぐにリソースを使い果たす可能性があります  
*  TwitterのAPI仕様により、100以上のリツイートが取れない模様。そのため、100リツイートになる前に再度実行する必要がありそう  

