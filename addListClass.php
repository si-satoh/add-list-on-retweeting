<?php

require __DIR__."/vendor/autoload.php";

use Abraham\TwitterOAuth\TwitterOAuth;
Class TwitterClass{

    private function getConnection(){
        $connection = new TwitterOAuth($this->consumer_key,$this->consumer_secret,$this->access_token,$this->access_token_secret);
        return $connection;
    }

    private $consumer_key, $consumer_secret, $access_token, $access_token_secret;
    public function __construct(){
        $json = file_get_contents('addListConf.json');
        $arr = json_decode($json,true);
        $this->consumer_key = $arr['consumer_key'];
        $this->consumer_secret = $arr['consumer_secret']; 
        $this->access_token = $arr['access_token'];
        $this->access_token_secret =$arr['access_token_secret'];
    }

    public function tweet($text="hello twitter"){
        $connection = $this->getConnection();
        //$content = $connection->get("account/verify_credentials");
        $datetimezone = new DateTimeZone("Asia/Tokyo"); 
        $datetime = new DateTime('now',$datetimezone);

        $nowText=$datetime->format('Y-m-d H:i:s');
        $req = $connection->post("statuses/update", array("status"=>$text." ".$nowText));

        // Twitterから返されたJSONをデコードする
        $result = $req;
        // JSONの配列（結果）を表示する
        return $result;

    }

   //TwitterのツイートIDをもとに、そのツイートのRTリストを取得
    public function getRetweetInformationByTweetID($tweetID){
        $connection = $this->getConnection();
        return $connection->get("statuses/retweets/".$tweetID);
    }

    //リツイートを行う
    public function retweet($tweetID){
        $connection = $this->getConnection();
        return $connection->post("statuses/retweet/".$tweetID);
    }

    public function getList(){
        $connection = $this->getConnection();
        return $connection->post("members/create",array('list_id'=>'',''));
    }

    //指定したリストにユーザーを登録
    public function addList($userID,$screenName,$listID){
        $connection = $this->getConnection();
        return $connection->post("lists/members/create",array('list_id'=>$listID,'user_id'=>$userID,'screen_name'=>$screenName));
    }
}
